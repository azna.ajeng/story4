from django.shortcuts import render
from django.http import HttpResponse

def index(request):
	return render(request, "index.html")

def about(request):
	return render(request,"about.html")

def profile(request):
	return render(request,"profile.html")

def reach (request):
	return render(request,"reach.html")
